import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  StyleSheet,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-maps';

import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  sectionContainer: {
    backgroundColor: Colors.lighter,
  },
});

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  findCurrentLocation = () => {
    Geolocation.getCurrentPosition((position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;

      this.setState({
        latitude,
        longitude,
      });
    });
  };

  requestLocationPermission = async () => {
    console.log('request permissions');
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'Map App needs access to your location ' +
            'to locate nearest station.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
        this.findCurrentLocation();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  render() {
    return (
      <View>
        <View>
          <TouchableOpacity onPress={this.requestLocationPermission}>
            <Text>Map Location</Text>
            <Text>{this.state.latitude}</Text>
            <Text>{this.state.longitude}</Text>
          </TouchableOpacity>
        </View>
        <View>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />
        </View>
      </View>
    );
  }
}

export default Map;
