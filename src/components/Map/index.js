import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

import MapMarker from '../MapMarker';
import UserLocationMarker from '../UserLocationMarker';
import Filter from '../../assets/Filter_BTN';
import Location from '../../assets/Location_BTN';
import QRCode from '../../assets/QRCode_BTN';

import styles from './styles';

const getZoomLevel = function (latitude) {
  if (latitude === 0) {
    return 0;
  }

  return 20;
};

const LOCATION_DELTA = 0.005;

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      currentRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
    };

    this.onClickMarker = this.onClickMarker.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.handleOnRegionChange = this.handleOnRegionChange.bind(this);
    this.animateToCurrentLocation = this.animateToCurrentLocation.bind(this);
  }

  componentDidMount() {
    this.animateToCurrentLocation();
  }

  findCurrentLocation = async () => {
    // If permissions are not givin then do not move forward
    if (!this.props.locationPermissionsGranted) {
      return;
    }

    Geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        };
        this.setState({
          currentRegion: region,
          region,
        });
      },
      (error) => console.log(error.message),
      {enableHighAccuracy: false},
    );
  };

  onClickMarker(latitude, longitude, title, description) {
    const region = {latitude, longitude, title, description};

    this.setState({
      region,
    });

    this.map.animateToRegion({
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: LOCATION_DELTA,
      longitudeDelta: LOCATION_DELTA,
    });
  }

  updateLocation(events) {
    if (events && events.nativeEvent && events.nativeEvent.coordinate) {
      const region = {
        latitude: events.nativeEvent.coordinate.latitude,
        longitude: events.nativeEvent.coordinate.longitude,
      };
      this.setState({
        currentRegion: region,
      });
    }
  }

  handleOnRegionChange(region) {
    this.setState({
      region,
    });
  }

  animateToCurrentLocation = async () => {
    await this.findCurrentLocation();
    const {currentRegion} = this.state;

    this.map.animateToRegion({
      latitude: currentRegion.latitude,
      longitude: currentRegion.longitude,
      latitudeDelta: LOCATION_DELTA,
      longitudeDelta: LOCATION_DELTA,
    });
  };

  render() {
    const {coordinates, locationPermissionsGranted} = this.props;
    const {region, currentRegion} = this.state;

    return (
      <View style={styles.container}>
        <MapView
          ref={(map) => {
            this.map = map;
          }}
          style={styles.map}
          initialRegion={{
            latitude: currentRegion.latitude,
            longitude: currentRegion.longitude,
            latitudeDelta: currentRegion.latitudeDelta || LOCATION_DELTA,
            longitudeDelta: currentRegion.longitudeDelta || LOCATION_DELTA,
          }}
          showsUserLocation={true}
          onRegionChange={this.handleOnRegionChange}
          onRegionChangeComplete={this.handleOnRegionChange}
          onUserLocationChange={this.updateLocation}
          maxZoomLevel={getZoomLevel(region.latitude)}>
          {coordinates.map((marker, index) => (
            <MapMarker
              key={index}
              latitude={marker.latitude}
              longitude={marker.longitude}
              title={marker.name}
              description={marker.site}
              onClickMarker={this.onClickMarker}
            />
          ))}
          {locationPermissionsGranted && (
            <UserLocationMarker
              latitude={currentRegion.latitude}
              longitude={currentRegion.longitude}
              onClickMarker={this.onClickMarker}
            />
          )}
        </MapView>
        <View style={styles.actionButtonsContainer}>
          <Filter />
          <QRCode />
          <TouchableOpacity onPress={this.animateToCurrentLocation}>
            <Location />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Map;
