import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    flex: 1,
  },
  actionButtonsContainer: {
    alignItems: 'flex-end',
    top: 250,
  },
});
