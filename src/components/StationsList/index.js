/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, FlatList} from 'react-native';

import Station from '../Station';

import styles from './styles';

class StationsList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {coordinates} = this.props;

    return (
      <View style={styles.container}>
        <FlatList
            data={coordinates}
            renderItem={({ item }) => <Station data={item} />}
            keyExtractor={(item, index) => `${index}`}
          />
      </View>
    );
  }
}

export default StationsList;
