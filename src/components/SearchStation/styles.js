import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  searchBar: {
    flex: 1,
    padding: 16,
  },
  stationsList: {
    flex: 6,
  },
  miniComponent: {
    position: 'absolute',
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
});
