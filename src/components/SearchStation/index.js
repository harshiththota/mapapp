import React, {Component} from 'react';
import {View} from 'react-native';

import SearchBar from '../SearchBar';
import StationsList from '../StationsList';

import styles from './styles';

class SearchStation extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {coordinates} = this.props;

    return (
      <View style={styles.miniComponent}>
        <View style={styles.searchBar}>
          <SearchBar />
        </View>
        <View style={styles.stationsList}>
          <StationsList coordinates={coordinates} />
        </View>
      </View>
    );
  }
}

export default SearchStation;
