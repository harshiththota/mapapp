import React, {Component} from 'react';
import {View} from 'react-native';
import {Marker} from 'react-native-maps';

import UserLocation from '../../assets/UserLocation';

import styles from './styles';

class UserLocationMarker extends Component {
  constructor(props) {
    super(props);

    this.onPressMarker = this.onPressMarker.bind(this);
  }

  onPressMarker() {
    const {latitude, longitude, title, description} = this.props;
    this.props.onClickMarker(latitude, longitude, title, description);
  }

  render() {
    const {latitude, longitude} = this.props;
    return (
      <Marker
        coordinate={{
          longitude: longitude,
          latitude: latitude,
        }}
        anchor={{x: 0.1, y: 0.4}}
        centerOffset={{x: 32, y: 25}}
        title={'Current Location'}
        description={'Site'}
        onPress={this.onPressMarker}>
        <View style={styles.pointerContainer}>
          <UserLocation />
        </View>
      </Marker>
    );
  }
}

export default UserLocationMarker;
