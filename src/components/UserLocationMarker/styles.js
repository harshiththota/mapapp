import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  pointerContainer: {
    flexDirection: 'row',
    width: 100,
    height: 100,
  },
});
