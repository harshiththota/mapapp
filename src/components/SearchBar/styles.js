import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  searchBar: {
    backgroundColor: '#F8F8F8',
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    width: 350,
    height: 44,
  },
});
