import React, {Component} from 'react';
import {View} from 'react-native';
import {Item, Input} from 'native-base';

import styles from './styles';

class SearchBar extends Component {
  render() {
    return (
      <View style={styles.searchBar}>
        <Item>
          <Input placeholder="Find Charging Station" />
        </Item>
      </View>
    );
  }
}

export default SearchBar;
