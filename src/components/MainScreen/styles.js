import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  sectionContainer: {
    flex: 1,
  },
});
