import React from 'react';
import {View} from 'react-native';

import Styles from './styles';

import MapScreen from '../MapScreen';

const App = () => {
  return (
    <View style={Styles.sectionContainer}>
      <MapScreen />
    </View>
  );
};

export default App;
