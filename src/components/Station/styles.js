import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  stationCard: {
    backgroundColor: '#FFFFFF',
    borderRadius: 12,
    margin: 16,
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  cardNameContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  cardTimeContainer: {},
  descriptionContainer: {
    marginLeft: 20,
  },
  nameContainer: {
    marginLeft: 20,
  },
});
