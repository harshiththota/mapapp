import React, {Component} from 'react';
import {View, Text} from 'react-native';

import Ellipse from '../../assets/Ellipse';

import styles from './styles';

class Station extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {data} = this.props;

    return (
      <View style={styles.stationCard}>
        <View style={styles.cardHeader}>
          <View style={styles.cardNameContainer}>
            <Ellipse />
            <Text>{data.name}</Text>
          </View>
          <View style={styles.cardTimeContainer}>
            <Text>{'10 mins'}</Text>
          </View>
        </View>
        <View style={styles.nameContainer}>
          <Text>{data.site}</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text>{data.latitude}</Text>
          <Text>{data.longitude}</Text>
        </View>
      </View>
    );
  }
}

export default Station;
