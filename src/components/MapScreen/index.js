import React, {Component} from 'react';
import {View} from 'react-native';
import {PERMISSIONS, RESULTS, requestMultiple} from 'react-native-permissions';
import SwipeUpDown from 'react-native-swipe-up-down';

import Map from '../Map';
import SampleChargingStations from '../../utils/SampleChargingStations';
import SearchStation from '../SearchStation';
import SearchBar from '../SearchBar';

import styles from './styles';

class MapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coordinates: [],
      station: {},
      locationPermissionsGranted: false,
    };
  }

  componentDidMount() {
    this.checkPermissions();
    this.getCoordinates();
  }

  getCoordinates() {
    const coordinates = SampleChargingStations(
      17.361806522191,
      78.47465498372912,
    );

    this.setState({
      coordinates,
    });
  }

  checkPermissions = async () => {
    try {
      const results = await requestMultiple([
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      ]);

      if (
        results[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === RESULTS.GRANTED ||
        results[PERMISSIONS.IOS.LOCATION_WHEN_IN_USE] === RESULTS.GRANTED
      ) {
        this.setState({
          locationPermissionsGranted: true,
        });
      } else {
        this.setState({
          locationPermissionsGranted: false,
        });
      }
    } catch (error) {
      console.log('error in check permissions');
    }
  };

  render() {
    const {coordinates, locationPermissionsGranted} = this.state;

    return (
      <View style={styles.mapContainer}>
        <Map
          coordinates={coordinates}
          locationPermissionsGranted={locationPermissionsGranted}
        />
        <SwipeUpDown
          hasRef={(ref) => (this.swipeUpDownRef = ref)}
          itemMini={<SearchBar />}
          itemFull={<SearchStation coordinates={coordinates} />}
          onMoveUp={() => this.swipeUpDownRef.showFull()}
          disablePressToShow={false}
          swipeHeight={100}
        />
      </View>
    );
  }
}

export default MapScreen;
