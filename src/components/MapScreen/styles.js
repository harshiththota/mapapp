import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  sectionContainer: {
    backgroundColor: '#555',
  },
  mapFooter: {
    position: 'absolute',
    width: '100%',
    backgroundColor: '#FFFFFF',
    top: 464,
    bottom: 0,
    flex: 1,
  },
  mapContainer: {
    borderWidth: 1,
    borderColor: '#AAA',
    flex: 1,
  },
});
