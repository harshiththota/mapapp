export default (lat, long) => [
  {
    name: 'StationName01',
    site: 'Site01',
    latitude: lat + 0.01,
    longitude: long + -0.02,
  },
  {
    name: 'StationName02',
    site: 'Site02',
    latitude: lat + 0.04,
    longitude: long + -0.03,
  },
];
