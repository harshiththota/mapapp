==== How to integrate google maps into the app

1. Create Google API key and enable the following 
Maps
GeoCoding

--> For android
1. Add location permissions in manifest file
2. Add meta data along with the API key in manifest file
3. Update app/build.gradle with android play services
4. Update android/build.gradle with google play and maps versions
--> iOS
1. Add react-native-maps modules in Pod file
2. Update Info.plist to access locations in ios 